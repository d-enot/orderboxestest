package com.test.sergeyklymenko.orderboxestest.source;

import com.test.sergeyklymenko.orderboxestest.error.InvalidUserData;
import com.test.sergeyklymenko.orderboxestest.order.domain.entity.User;

import java.util.Set;

import io.reactivex.Single;
import io.reactivex.annotations.NonNull;
import io.reactivex.annotations.Nullable;

public interface UserDataSource {

    @Nullable
    Single<Set<User>> getUsers();

    @Nullable
    Single<User> getUser(@NonNull String email);

    void saveUser(@NonNull User user) throws InvalidUserData;

    void updateUser(@NonNull User user);
}

package com.test.sergeyklymenko.orderboxestest.order;

import android.os.Bundle;

import com.test.sergeyklymenko.orderboxestest.R;

import javax.inject.Inject;

import dagger.Lazy;
import dagger.android.support.DaggerAppCompatActivity;

public class OrderActivity extends DaggerAppCompatActivity {

    @Inject
    Lazy<OrderFragment> fragmentProvider;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order);

        OrderFragment orderFragment =
                (OrderFragment) getSupportFragmentManager().findFragmentById(R.id.frame);

        if (orderFragment == null) {
            orderFragment = fragmentProvider.get();
            getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.frame, orderFragment).commit();
        }
    }
}

package com.test.sergeyklymenko.orderboxestest.di;

import android.app.Application;
import android.content.Context;
import android.support.annotation.NonNull;

import com.test.sergeyklymenko.orderboxestest.order.domain.usecase.SaveUser;
import com.test.sergeyklymenko.orderboxestest.source.BoxDataSource;
import com.test.sergeyklymenko.orderboxestest.source.BoxReposytory;
import com.test.sergeyklymenko.orderboxestest.source.OrderDataSource;
import com.test.sergeyklymenko.orderboxestest.source.OrderRepository;
import com.test.sergeyklymenko.orderboxestest.source.UserDataSource;
import com.test.sergeyklymenko.orderboxestest.source.UserRepositoty;
import com.test.sergeyklymenko.orderboxestest.source.local.UserLocalDataSource;
import com.test.sergeyklymenko.orderboxestest.source.remote.UserRemoteDataSource;

import java.io.File;

import javax.inject.Singleton;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;

@Module
public abstract class AppModule {

    @Binds
    abstract Context bindContext(Application application);

    @Provides
    @Singleton
    public static File getCacheDir(@NonNull Context context) {
        final File external = context.getExternalCacheDir();
        return external != null ? external : context.getCacheDir();
    }

    @Provides
    @Singleton
    public static UserLocalDataSource getUserLocalDataSource(@NonNull File cacheDir) {
        return new UserLocalDataSource(cacheDir);
    }

    @Provides
    @Singleton
    public static UserRemoteDataSource getUserRemoteDataSource() {
        return new UserRemoteDataSource();
    }

    @Provides
    @Singleton
    public static BoxDataSource getBoxDataSource(@NonNull Context context) {
        return new BoxReposytory(context);
    }

    @Provides
    @Singleton
    public static OrderDataSource getOrderDataSource(@NonNull File cacheDir) {
        return new OrderRepository(cacheDir);
    }
}


package com.test.sergeyklymenko.orderboxestest.source;

import com.test.sergeyklymenko.orderboxestest.order.domain.entity.Order;

import io.reactivex.Completable;

public interface OrderDataSource {

    Completable saveOrder(Order order);
}

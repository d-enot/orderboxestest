package com.test.sergeyklymenko.orderboxestest.order.domain.usecase;

import com.test.sergeyklymenko.orderboxestest.order.domain.entity.User;
import com.test.sergeyklymenko.orderboxestest.source.UserDataSource;
import com.test.sergeyklymenko.orderboxestest.source.UserRepositoty;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Single;

@Singleton
public class GetUser {

    UserDataSource repository;

    @Inject
    public GetUser(UserRepositoty repository) {
        this.repository = repository;
    }

    public Single<User> execute(String email) {
        return repository.getUser(email);
    }
}

package com.test.sergeyklymenko.orderboxestest.order.domain.usecase;

import com.test.sergeyklymenko.orderboxestest.order.domain.entity.BoxType;
import com.test.sergeyklymenko.orderboxestest.source.BoxDataSource;
import com.test.sergeyklymenko.orderboxestest.source.BoxReposytory;

import java.util.Set;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Single;

@Singleton
public class GetBoxTypes {

    private BoxDataSource reposytory;

    @Inject
    public GetBoxTypes(BoxDataSource reposytory) {
        this.reposytory = reposytory;
    }

    public Single<Set<BoxType>> execute() {
        return reposytory.getBoxTypes();
    }
}

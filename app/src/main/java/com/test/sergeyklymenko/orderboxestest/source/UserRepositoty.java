package com.test.sergeyklymenko.orderboxestest.source;

import android.annotation.SuppressLint;

import com.test.sergeyklymenko.orderboxestest.error.InvalidUserData;
import com.test.sergeyklymenko.orderboxestest.order.domain.entity.User;
import com.test.sergeyklymenko.orderboxestest.source.local.UserLocalDataSource;
import com.test.sergeyklymenko.orderboxestest.source.remote.UserRemoteDataSource;

import java.util.Set;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

@Singleton
public class UserRepositoty implements UserDataSource {

    private UserDataSource localDataSource;
    private UserDataSource remoteDataSource;
    private Set<User> users;

    @Inject
    public UserRepositoty(UserLocalDataSource localDataSource, UserRemoteDataSource remoteDataSource) {
        this.localDataSource = localDataSource;
        this.remoteDataSource = remoteDataSource;
        getCachedUsers();
    }

    @Override
    public Single<Set<User>> getUsers() {
        return localDataSource.getUsers();
    }

    @Override
    public Single<User> getUser(String email) {
        if (users != null) {
            for (User user : users) {
                if (user.getEmail().equals(email)) {
                    return Single.just(user);
                }
            }
        }
        return null;
    }

    @Override
    public void saveUser(User user) throws InvalidUserData {
        remoteDataSource.saveUser(user);
        localDataSource.saveUser(user);
        users.add(user);
    }

    @Override
    public void updateUser(User user) {

    }

    @SuppressLint("CheckResult")
    private void getCachedUsers() {
        localDataSource.getUsers()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(usersList -> users = usersList);
    }
}

package com.test.sergeyklymenko.orderboxestest.source;

import com.test.sergeyklymenko.orderboxestest.order.domain.entity.BoxType;

import java.util.Set;

import io.reactivex.Single;
import io.reactivex.annotations.Nullable;

public interface BoxDataSource {

    @Nullable
    Single<Set<BoxType>> getBoxTypes();
}

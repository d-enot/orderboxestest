package com.test.sergeyklymenko.orderboxestest.order;

import com.test.sergeyklymenko.orderboxestest.di.ActivityScoped;
import com.test.sergeyklymenko.orderboxestest.order.domain.entity.BoxType;
import com.test.sergeyklymenko.orderboxestest.order.domain.entity.User;
import com.test.sergeyklymenko.orderboxestest.order.domain.usecase.GetBoxTypes;
import com.test.sergeyklymenko.orderboxestest.order.domain.usecase.GetUser;
import com.test.sergeyklymenko.orderboxestest.order.domain.usecase.SaveOrder;
import com.test.sergeyklymenko.orderboxestest.order.domain.usecase.SaveUser;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

@ActivityScoped
public class OrderPresenter implements OrderContract.Presenter {

    private SaveUser saveUser;
    private GetUser getUser;
    private SaveOrder saveOrder;
    private GetBoxTypes getBoxTypes;
    private OrderContract.View view;
    private CompositeDisposable disposable;
    private User currentUser;
    private BoxType[] boxTypesArray;
    private int currentItem = 0;
    private int currentItemColor = 0;

    @Inject
    public OrderPresenter(SaveUser saveUser,
                          GetBoxTypes getBoxTypes,
                          SaveOrder saveOrder,
                          GetUser getUser) {
        this.saveUser = saveUser;
        this.getBoxTypes = getBoxTypes;
        this.saveOrder = saveOrder;
        this.getUser = getUser;
        disposable = new CompositeDisposable();
    }

    @Override
    public void setPickedBox(int index) {
        currentItem = index;
        if (view != null) {
            view.setBoxesColors(boxTypesArray[index].getColors());
        }
    }

    @Override
    public void setPickedBoxColor(int index) {
        currentItem = index;
    }

    @Override
    public void takeView(OrderContract.View view) {
        this.view = view;
        disposable.add(getBoxTypes.execute().subscribe(boxTypes -> {
            boxTypesArray = boxTypes.toArray(new BoxType[boxTypes.size()]);
            if (view != null) {
                view.setBoxesTypes(boxTypesArray);
                view.setBoxesColors(boxTypesArray[0].getColors());
            }
        }, error -> showError(error.getMessage())));
    }

    @Override
    public void dropView() {
        this.view = null;
        if (!disposable.isDisposed()) {
            disposable.dispose();
        }
    }

    @Override
    public void saveUser(String name, String email) {
        boolean isError = false;
        if (name.isEmpty()) {
            isError = true;
            if (view != null) {
                view.setUserNameError(true);
            }
        }
        if (email.isEmpty()) {
            isError = true;
            if (view != null) {
                view.setUserEmailError(true);
            }
        }
        if (isError) {
            return;
        }
        if (view != null) {
            view.showProgressBar(true);
        }
        disposable.add(saveUser.execute(name, email)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doFinally(() -> {
                    if (view != null) {
                        view.showProgressBar(false);
                    }
                })
                .doOnComplete(() -> {
                    if (view != null) {
                        view.showOrderView(true);
                    }
                })
                .subscribe(() -> getUser(email), error -> showError(error.getMessage())));
    }

    @Override
    public void saveOrder() {
        BoxType boxType = boxTypesArray[currentItem];
        disposable.add(saveOrder.execute(boxType, currentUser,
                boxType.getColors()[currentItemColor])
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(() -> {
                    if (view != null) {
                        view.showOrderSaved();
                    }
                }, error -> showError(error.getMessage())));
    }

    @Override
    public void showError(String errorMsg) {
        if (view != null) {
            view.showError(errorMsg);
        }
    }

    private void getUser(String email) {
        disposable.add(getUser.execute(email)
                .subscribeOn(Schedulers.io())
                .subscribe(user -> currentUser = user));
    }
}

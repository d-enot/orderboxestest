package com.test.sergeyklymenko.orderboxestest.source;

import android.content.Context;

import com.test.sergeyklymenko.orderboxestest.order.domain.entity.BoxType;
import com.test.sergeyklymenko.orderboxestest.utils.Constants;
import com.test.sergeyklymenko.orderboxestest.utils.GsonConverter;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashSet;
import java.util.Set;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Single;

@Singleton
public class BoxReposytory implements BoxDataSource {

    private Context context;

    @Inject
    public BoxReposytory(Context context) {
        this.context = context;
    }

    @Override
    public Single<Set<BoxType>> getBoxTypes() {
        return Single.just(getBoxTypesFromAssets());
    }

    private Set<BoxType> getBoxTypesFromAssets() {
        Set<BoxType> boxTypes = new HashSet<>();
        BufferedReader reader;
        StringBuilder rawData = new StringBuilder();
        String line;
        try {
            reader = new BufferedReader(new InputStreamReader(context.getAssets().open(Constants.BOX_TYPES_FILE)));
            while ((line = reader.readLine()) != null) {
                rawData.append(line);
            }
            reader.close();
            boxTypes = GsonConverter.convertStringToBoxTypesSet(rawData.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return boxTypes;
    }
}

package com.test.sergeyklymenko.orderboxestest.source;

import com.test.sergeyklymenko.orderboxestest.order.domain.entity.Order;
import com.test.sergeyklymenko.orderboxestest.utils.Constants;
import com.test.sergeyklymenko.orderboxestest.utils.GsonConverter;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Completable;

@Singleton
public class OrderRepository implements OrderDataSource {

    private File fileDirectory;

    @Inject
    public OrderRepository(File fileDirectory) {
        this.fileDirectory = fileDirectory;
    }

    @Override
    public Completable saveOrder(Order order) {
        return Completable.fromAction(() -> saveOrderLocal(order));
    }

    private void saveOrderLocal(Order order) {
        File tempFile = new File(fileDirectory.getPath(), Constants.USERS_DATA_TABLE_NAME);
        String rawData = GsonConverter.convertOrderToString(order);
        FileWriter writer;
        try {
            writer = new FileWriter(tempFile);
            writer.write(rawData);
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

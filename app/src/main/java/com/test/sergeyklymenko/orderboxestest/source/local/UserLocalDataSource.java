package com.test.sergeyklymenko.orderboxestest.source.local;

import android.util.Log;

import com.test.sergeyklymenko.orderboxestest.order.domain.entity.User;
import com.test.sergeyklymenko.orderboxestest.source.UserDataSource;
import com.test.sergeyklymenko.orderboxestest.utils.Constants;
import com.test.sergeyklymenko.orderboxestest.utils.GsonConverter;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import io.reactivex.Single;
import io.reactivex.annotations.NonNull;

public class UserLocalDataSource implements UserDataSource {

    private File fileDirectory;

    public UserLocalDataSource(File fileDirectory) {
        this.fileDirectory = fileDirectory;
    }

    @Override
    public Single<Set<User>> getUsers() {
        return Single.just(readUsersFromStorage());
    }

    @Override
    public Single<User> getUser(String email) {

        return null;
    }

    @Override
    public void saveUser(User user) {
        putUserInStorage(user);
    }

    @Override
    public void updateUser(User user) {

    }

    @NonNull
    private Set<User> readUsersFromStorage() {
        Set<User> users = new HashSet<>();
        File tempFile = new File(fileDirectory.getPath(), Constants.USERS_DATA_TABLE_NAME);
        StringBuilder rawData = new StringBuilder();
        String line;
        FileReader fReader;
        BufferedReader bReader;
        try {
            fReader = new FileReader(tempFile);
            bReader = new BufferedReader(fReader);
            while ((line = bReader.readLine()) != null) {
                rawData.append(line);
            }
            fReader.close();
            bReader.close();
            users = GsonConverter.convertStringToUserSet(rawData.toString());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return users;
    }

    private void putUserInStorage(User user) {
        File tempFile = new File(fileDirectory.getPath(), Constants.USERS_DATA_TABLE_NAME);
        Set<User> users = readUsersFromStorage();
        users.add(user);
        String rawData = GsonConverter.convertUserSetToString(users);
        FileWriter writer;
        try {
            writer = new FileWriter(tempFile);
            writer.write(rawData);
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

package com.test.sergeyklymenko.orderboxestest.source.remote;

import com.test.sergeyklymenko.orderboxestest.error.InvalidUserData;
import com.test.sergeyklymenko.orderboxestest.order.domain.entity.User;
import com.test.sergeyklymenko.orderboxestest.source.UserDataSource;
import com.test.sergeyklymenko.orderboxestest.utils.EmailValidator;

import java.util.Set;

import io.reactivex.Single;

public class UserRemoteDataSource implements UserDataSource {

    public UserRemoteDataSource() {
    }

    @Override
    public Single<Set<User>> getUsers() {
        return null;
    }

    @Override
    public Single<User> getUser(String email) {
        return null;
    }

    @Override
    public void saveUser(User user) throws InvalidUserData {
        if (validateEmail(user.getEmail())) {
            //this is mock, so we do nothing
        } else {
            throw new InvalidUserData("Invalid email addres");
        }
    }

    @Override
    public void updateUser(User user) {

    }

    private boolean validateEmail(String email) {
        EmailValidator emailValidator = new EmailValidator();
        return emailValidator.validateEmail(email);
    }
}

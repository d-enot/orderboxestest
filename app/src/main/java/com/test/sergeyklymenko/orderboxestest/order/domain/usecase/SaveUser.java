package com.test.sergeyklymenko.orderboxestest.order.domain.usecase;

import com.test.sergeyklymenko.orderboxestest.order.domain.entity.User;
import com.test.sergeyklymenko.orderboxestest.source.UserDataSource;
import com.test.sergeyklymenko.orderboxestest.source.UserRepositoty;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Completable;
import io.reactivex.annotations.NonNull;

@Singleton
public class SaveUser {

    private UserDataSource repositoty;

    @Inject
    public SaveUser(UserRepositoty repositoty) {
        this.repositoty = repositoty;
    }

    public Completable execute(@NonNull String userName, @NonNull String userEmail) {
        User user = new User(userName, userEmail);
        return Completable.fromAction(() -> repositoty.saveUser(user));
    }
}

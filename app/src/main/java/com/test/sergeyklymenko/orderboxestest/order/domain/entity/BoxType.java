package com.test.sergeyklymenko.orderboxestest.order.domain.entity;

public class BoxType {

    private String name;
    private int[] params;
    private String[] colors;

    public BoxType(String name, int[] params, String[] colors) {
        this.name = name;
        this.params = params;
        this.colors = colors;
    }

    public String getName() {
        return name;
    }

    public int[] getParams() {
        return params;
    }

    public String[] getColors() {
        return colors;
    }

    @Override
    public String toString() {
        return name + " " + params[0] + "X" + params[1] + "X" + params[2];
    }
}

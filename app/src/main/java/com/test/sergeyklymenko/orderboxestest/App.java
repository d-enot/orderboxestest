package com.test.sergeyklymenko.orderboxestest;

import com.test.sergeyklymenko.orderboxestest.di.AppComponent;
import com.test.sergeyklymenko.orderboxestest.di.DaggerAppComponent;

import dagger.android.AndroidInjector;
import dagger.android.support.DaggerApplication;

public class App extends DaggerApplication {

    @Override
    protected AndroidInjector<? extends DaggerApplication> applicationInjector() {
        AppComponent appComponent = DaggerAppComponent.builder().application(this).build();
        appComponent.inject(this);
        return appComponent;
    }
}

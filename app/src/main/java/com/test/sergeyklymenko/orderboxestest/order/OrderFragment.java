package com.test.sergeyklymenko.orderboxestest.order;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import com.test.sergeyklymenko.orderboxestest.R;
import com.test.sergeyklymenko.orderboxestest.di.ActivityScoped;
import com.test.sergeyklymenko.orderboxestest.dialog.ErrorDialogFragment;
import com.test.sergeyklymenko.orderboxestest.order.domain.entity.BoxType;

import javax.inject.Inject;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;
import dagger.android.support.DaggerFragment;

@ActivityScoped
public class OrderFragment extends DaggerFragment implements OrderContract.View, AdapterView.OnItemSelectedListener {

    @Inject OrderPresenter presenter;

    @BindView(R.id.user_name)
    EditText userName;

    @BindView(R.id.user_name_layout)
    TextInputLayout userNameLayout;

    @BindView(R.id.progress)
    ProgressBar progressBar;

    @BindView(R.id.user_email)
    EditText userEmail;

    @BindView(R.id.order_view)
    View orderView;

    @BindView(R.id.box_chooser)
    Spinner boxTypesSpinner;

    @BindView(R.id.box_color_chooser)
    Spinner boxCollorSpinner;

    @BindView(R.id.user_email_layout)
    TextInputLayout userEmailLayout;

    @BindString(R.string.error_dialog_positive_btn)
    String positiveBtnTxt;

    @BindString(R.string.error_dialog_title)
    String errorTitle;

    @BindString(R.string.user_label_name_error)
    String errorUserNameTxt;

    @BindString(R.string.user_label_email_error)
    String errorUserEmailTxt;

    @BindString(R.string.order_saved_msg)
    String orderSavedTxt;


    @Inject
    public OrderFragment() {
        // Dagger requires empty public constructor
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_order, container, false);
        ButterKnife.bind(this, view);
        presenter.takeView(this);
        boxTypesSpinner.setOnItemSelectedListener(this);
        boxCollorSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                presenter.setPickedBoxColor(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                //do nothing
            }
        });
        return view;
    }

    @OnClick(R.id.signup_btn)
    public void OnClickSignUpBtn() {
        presenter.saveUser(userName.getText().toString(), userEmail.getText().toString());
    }

    @OnClick(R.id.make_order_btn)
    public void OnClickMakeOrderBtn() {
        presenter.saveOrder();
    }

    @OnTextChanged(R.id.user_name)
    public void onNameTxtChange(CharSequence text) {
        if (text.length() > 0) {
            setUserNameError(false);
        }
    }

    @OnTextChanged(R.id.user_email)
    public void onEmailTxtChange(CharSequence text) {
        if (text.length() > 0) {
            setUserEmailError(false);
        }
    }

    @Override
    public void showProgressBar(boolean show) {
        if (show) {
            progressBar.setVisibility(View.VISIBLE);
        } else {
            progressBar.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public void showOrderView(boolean show) {
        if (show) {
            orderView.setVisibility(View.VISIBLE);
        } else {
            orderView.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public void setBoxesTypes(BoxType[] boxesTypes) {
        if (boxesTypes == null) {
            return;
        }
        ArrayAdapter<BoxType> adapter = new ArrayAdapter<>(getActivity(),
                android.R.layout.simple_spinner_item,
                boxesTypes);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        boxTypesSpinner.setAdapter(adapter);
    }

    @Override
    public void setBoxesColors(String[] colors) {
        if (colors == null) {
            return;
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(),
                android.R.layout.simple_spinner_item,
                colors);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        boxCollorSpinner.setAdapter(adapter);
    }

    @Override
    public void setUserNameError(boolean show) {
        if (show) {
            userNameLayout.setError(errorUserNameTxt);
        } else {
            userNameLayout.setErrorEnabled(false);
        }
    }

    @Override
    public void setUserEmailError(boolean show) {
        if (show) {
            userEmailLayout.setError(errorUserEmailTxt);
        } else {
            userEmailLayout.setErrorEnabled(false);
        }
    }

    @Override
    public void showError(String errorMsg) {
        ErrorDialogFragment.show(getFragmentManager(),
                errorTitle,
                errorMsg,
                positiveBtnTxt,
                null);
    }

    @Override
    public void showOrderSaved() {
        Toast.makeText(getContext(), orderSavedTxt, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onDetach() {
        presenter.dropView();
        super.onDetach();
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        presenter.setPickedBox(position);
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        //do nothing
    }
}

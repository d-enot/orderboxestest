package com.test.sergeyklymenko.orderboxestest.order.domain.entity;

public class Order {

    private User user;
    private Box box;

    public Order(User user, Box box) {
        this.user = user;
        this.box = box;
    }

    public User getUser() {
        return user;
    }

    public Box getBox() {
        return box;
    }
}

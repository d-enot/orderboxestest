package com.test.sergeyklymenko.orderboxestest.order.domain.entity;

import android.support.annotation.NonNull;

import java.util.Objects;

public class User implements Comparable<User> {

    private String name;
    private String email;

    public User(String name, String email) {
        this.name = name;
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(email);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        User user = (User) obj;
        return Objects.equals(email, user.getEmail());
    }

    @Override
    public int compareTo(@NonNull User o) {
        return email.compareTo(o.getEmail());
    }
}

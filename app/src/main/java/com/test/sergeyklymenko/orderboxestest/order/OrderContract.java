package com.test.sergeyklymenko.orderboxestest.order;

import com.test.sergeyklymenko.orderboxestest.order.domain.entity.BoxType;

import java.util.Set;

public interface OrderContract {

    interface View {

        void showProgressBar(boolean show);

        void showOrderView(boolean show);

        void setBoxesTypes(BoxType[] boxesTypes);

        void setBoxesColors(String[] colors);

        void setUserNameError(boolean show);

        void setUserEmailError(boolean show);

        void showError(String errorMsg);

        void showOrderSaved();

    }

    interface Presenter {

        void setPickedBox(int index);

        void setPickedBoxColor(int index);

        void takeView(View view);

        void dropView();

        void saveUser(String name, String email);

        void saveOrder();

        void showError(String errorMsg);
    }
}

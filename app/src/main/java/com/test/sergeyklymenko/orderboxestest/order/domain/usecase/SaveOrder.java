package com.test.sergeyklymenko.orderboxestest.order.domain.usecase;

import com.test.sergeyklymenko.orderboxestest.order.domain.entity.Box;
import com.test.sergeyklymenko.orderboxestest.order.domain.entity.BoxType;
import com.test.sergeyklymenko.orderboxestest.order.domain.entity.Order;
import com.test.sergeyklymenko.orderboxestest.order.domain.entity.User;
import com.test.sergeyklymenko.orderboxestest.source.OrderDataSource;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Completable;

@Singleton
public class SaveOrder {

    private OrderDataSource repository;

    @Inject
    public SaveOrder(OrderDataSource repository) {
        this.repository = repository;
    }

    public Completable execute(BoxType boxType, User user, String color) {
        int width = boxType.getParams()[0];
        int length = boxType.getParams()[1];
        int height = boxType.getParams()[2];
        Box box = new Box(boxType.getName(), width, length, height, color);
        Order order = new Order(user, box);
        return repository.saveOrder(order);
    }
}

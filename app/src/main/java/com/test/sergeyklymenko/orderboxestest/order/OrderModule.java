package com.test.sergeyklymenko.orderboxestest.order;

import com.test.sergeyklymenko.orderboxestest.di.ActivityScoped;
import com.test.sergeyklymenko.orderboxestest.di.FragmentScoped;

import dagger.Binds;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class OrderModule {

    @FragmentScoped
    @ContributesAndroidInjector
    abstract OrderFragment imporFragment();

    @ActivityScoped
    @Binds
    abstract OrderContract.Presenter contactsPresenter(OrderPresenter contactsPresenter);
}

package com.test.sergeyklymenko.orderboxestest.order.domain.entity;

public class Box {

    private String name;
    private int width;
    private int length;
    private int height;
    private String color;

    public Box(String name, int width, int length, int height, String color) {
        this.name = name;
        this.width = width;
        this.length = length;
        this.height = height;
        this.color = color;
    }

    public String getName() {
        return "Box " + width + "x" + length + "x" + height + " " + color;
    }

    public int getWidth() {
        return width;
    }

    public int getLength() {
        return length;
    }

    public int getHeight() {
        return height;
    }

    public String getColor() {
        return color;
    }
}

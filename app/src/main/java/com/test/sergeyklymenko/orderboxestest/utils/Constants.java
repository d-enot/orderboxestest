package com.test.sergeyklymenko.orderboxestest.utils;

public final class Constants {

    public static final String USERS_DATA_TABLE_NAME = "users_table";
    public static final String ORDERS_DATA_TABLE_NAME = "orders_table";
    public static final String BOX_TYPES_FILE = "box_types";

    public Constants() {
        throw new UnsupportedOperationException();
    }
}

package com.test.sergeyklymenko.orderboxestest.error;

public class InvalidUserData extends Exception {
    public InvalidUserData(String message) {
        super(message);
    }
}

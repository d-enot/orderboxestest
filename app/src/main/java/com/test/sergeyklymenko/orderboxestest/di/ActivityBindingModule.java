package com.test.sergeyklymenko.orderboxestest.di;

import com.test.sergeyklymenko.orderboxestest.order.OrderActivity;
import com.test.sergeyklymenko.orderboxestest.order.OrderModule;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class ActivityBindingModule {
    @ActivityScoped
    @ContributesAndroidInjector(modules = OrderModule.class)
    abstract OrderActivity orderActivity();
}

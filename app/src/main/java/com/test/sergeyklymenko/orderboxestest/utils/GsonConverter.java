package com.test.sergeyklymenko.orderboxestest.utils;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.test.sergeyklymenko.orderboxestest.order.domain.entity.BoxType;
import com.test.sergeyklymenko.orderboxestest.order.domain.entity.Order;
import com.test.sergeyklymenko.orderboxestest.order.domain.entity.User;

import java.lang.reflect.Type;
import java.util.HashSet;
import java.util.Set;

import io.reactivex.annotations.NonNull;

import static com.google.common.base.Preconditions.checkNotNull;

public final class GsonConverter {

    public static Set<User> convertStringToUserSet(@NonNull String rawData) {
        checkNotNull(rawData);
        Gson gson = new Gson();
        Type userSetType = new TypeToken<HashSet<User>>(){}.getType();
        return gson.fromJson(rawData, userSetType);
    }

    public static Set<BoxType> convertStringToBoxTypesSet(@NonNull String rawData) {
        checkNotNull(rawData);
        Gson gson = new Gson();
        Type userSetType = new TypeToken<HashSet<BoxType>>(){}.getType();
        return gson.fromJson(rawData, userSetType);
    }

    public static String convertUserSetToString(@NonNull Set<User> users) {
        checkNotNull(users);
        Gson gson = new Gson();
        return gson.toJson(users);
    }

    public static String convertOrderToString(@NonNull Order order) {
        checkNotNull(order);
        Gson gson = new Gson();
        return gson.toJson(order);
    }

    private GsonConverter() {
        throw new UnsupportedOperationException();
    }
}
